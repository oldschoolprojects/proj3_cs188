# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import mdp, util

from learningAgents import ValueEstimationAgent
import collections

class ValueIterationAgent(ValueEstimationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A ValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100):
        """
          Your value iteration agent should take an mdp on
          construction, run the indicated number of iterations
          and then act according to the resulting policy.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state, action, nextState)
              mdp.isTerminal(state)
        """
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter() # A Counter is a dict with default 0
        self.runValueIteration()

    # ====================================================================
    #                       Begin: Q1 Value Iteration
    # ====================================================================

    def runValueIteration(self):
        """
        Performs self.iterations amount of Bellman updates
        on all elements of self.values vector.
        """
        states = self.mdp.getStates()
        for _ in range(self.iterations):
            # We store this (K+1)th Bellman update in a separate dictionary,
            # and replace the Kth iteration when done.
            values_k1 = self.values.copy()

            for state in states:
                # Store Kth Bellman update in STATE.
                values_k1[state] = self.getMaxValue(state)

            self.values = values_k1

    def getMaxValue(self, state):
        """
        Helper function I made for runValueIteration.
        Returns Bellman update for STATE at some given iteration.
        """
        if self.mdp.isTerminal(state):
            return self.mdp.getReward(state, None, state)

        actions = self.mdp.getPossibleActions(state)

        # Loop over available actions from STATE.
        # Return max_a{\sum_sPrime[T * (R + \gamma * V_k(sPrime))]}.
        return max([ self.getQValue(state, action) for action in actions ])

    def getValue(self, state):
        """
          Return the value of the state (computed in __init__).
        """
        return self.values[state]

    def computeQValueFromValues(self, state, action):
        """
          Compute the Q-value of action in state from the
          value function stored in self.values.
        """
        averageOverSuccessors = 0
        transFunc = self.mdp.getTransitionStatesAndProbs(state, action)

        # Perform sPrime summation in Bellman update.
        for sPrime, T in transFunc:
            R = self.mdp.getReward(state, action, sPrime)
            averageOverSuccessors += T * (R + self.discount * self.values[sPrime])

        return averageOverSuccessors

    def computeActionFromValues(self, state):
        """
          The policy is the best action in the given state
          according to the values currently stored in self.values.

          You may break ties any way you see fit.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return None.
        """
        actions = self.mdp.getPossibleActions(state)
        if not actions:
            return None

        dictActionVal = {}
        for action in actions:
            dictActionVal[action] = self.getQValue(state, action)

        # Return action (key) corresponding to maximum value.
        return max(dictActionVal, key=dictActionVal.get)

    # ====================================================================
    #                       End: Q1 Value Iteration
    # ====================================================================

    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    def getAction(self, state):
        "Returns the policy at the state (no exploration)."
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)

class AsynchronousValueIterationAgent(ValueIterationAgent):
    """
        * Please read learningAgents.py before reading this.*

        An AsynchronousValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs cyclic value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 1000):
        """
          Your cyclic value iteration agent should take an mdp on
          construction, run the indicated number of iterations,
          and then act according to the resulting policy. Each iteration
          updates the value of only one state, which cycles through
          the states list. If the chosen state is terminal, nothing
          happens in that iteration.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state)
              mdp.isTerminal(state)
        """
        ValueIterationAgent.__init__(self, mdp, discount, iterations)

    def runValueIteration(self):
        states = self.mdp.getStates()
        state_i = 0
        state_len = len(states)

        for _ in xrange(self.iterations):
            state = states[state_i]
            self.values[state] = self.getMaxValue(state)
            state_i = (state_i + 1) % state_len

class PrioritizedSweepingValueIterationAgent(AsynchronousValueIterationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A PrioritizedSweepingValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs prioritized sweeping value iteration
        for a given number of iterations using the supplied parameters.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100, theta = 1e-5):
        """
          Your prioritized sweeping value iteration agent should take an mdp on
          construction, run the indicated number of iterations,
          and then act according to the resulting policy.
        """
        self.theta = theta
        ValueIterationAgent.__init__(self, mdp, discount, iterations)

    def runValueIteration(self):
        states          = self.mdp.getStates()
        predecessors    = {state: self.getPredecessors(state) for state in states}
        priorityQueue   = util.PriorityQueue()
        nonTermStates   = [state for state in states if not self.mdp.isTerminal(state)]

        # For each non-terminal state s, do:
        for s in nonTermStates:
            diff = abs(self.values[s] - self.getMaxQValue(s))
            priorityQueue.push(s, -diff)

        # For iteration in 0, 1, 2, ..., self.iterations - 1, do:
        for _ in range(self.iterations):
            if priorityQueue.isEmpty():
                return

            s = priorityQueue.pop()
            if s in nonTermStates:
                self.values[s] = self.getMaxValue(s)

            # For each predecessor p of s, do:
            for p in predecessors[s]:
                diff = abs(self.values[p] - self.getMaxQValue(p))
                if diff > self.theta:
                    priorityQueue.update(p, -diff)


    # ======================================================================
    # Below are helper functions I made solely for runValueIteration above.
    # ======================================================================

    def getPredecessors(self, s):
        res = set()
        allStates = self.mdp.getStates()
        # Loop over allStates to see if any lead to s.
        for state in allStates:
            sPrimes = self.getSPrimes(state)
            for poop in sPrimes:
                if poop == s:
                    res.add(state)
        return res

    def getSPrimes(self, state):
        actions = self.mdp.getPossibleActions(state)
        sPrimes = set()
        for action in actions:
            transFunc = self.mdp.getTransitionStatesAndProbs(state, action)
            possibleStates = [sPrime for sPrime, T in transFunc if T > 0]
            for butthole in possibleStates:
                sPrimes.add(butthole)
        return sPrimes

    def getMaxQValue(self, state):
        actions = self.mdp.getPossibleActions(state)
        return max([ self.getQValue(state, action) for action in actions ])


